# Verspaetung Transport - Interview Challenge

version 1.5.0 - 21/02/2023

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/verspaetung-transport.svg)](http://bitbucket.org/ivan-sanabria/verspaetung-transport/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/verspaetung-transport.svg)](http://bitbucket.org/ivan-sanabria/verspaetung-transport/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_verspaetung-transport&metric=alert_status)](https://sonarcloud.io/project/overview?id=Iván-Camilo-Sanabria-Rincón_verspaetung-transport)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_verspaetung-transport&metric=bugs)](https://sonarcloud.io/project/issues?resolved=false&types=BUG&id=Iván-Camilo-Sanabria-Rincón_verspaetung-transport)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_verspaetung-transport&metric=coverage)](https://sonarcloud.io/component_measures?id=Iván-Camilo-Sanabria-Rincón_verspaetung-transport&metric=coverage)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_verspaetung-transport&metric=ncloc)](https://sonarcloud.io/code?id=Iván-Camilo-Sanabria-Rincón_verspaetung-transport)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_verspaetung-transport&metric=sqale_index)](https://sonarcloud.io/component_measures?metric=Maintainability&id=Iván-Camilo-Sanabria-Rincón_verspaetung-transport)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_verspaetung-transport&metric=vulnerabilities)](https://sonarcloud.io/project/issues?resolved=false&types=VULNERABILITY&id=Iván-Camilo-Sanabria-Rincón_verspaetung-transport)

## Specifications

Please review the PDF file attached on the project.

## Requirements

- JDK 14.x
- Maven 3.6.3
- IDE for JAVA (Eclipse, Netbeans, IntelliJ).
- Postman 10.10.x
- JMeter 5.1.x
- Serverless 2.72.x
- [Docker 2.3.x](https://www.docker.com/products/docker-desktop)
- [EB CLI 3.18.x](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-install-advanced.html)

## Serverless Setup

In order to deploy the DynamoDB tables in AWS account is required to have an **AWS_ACCESS_KEY_ID** and **AWS_SECRET_ACCESS_KEY**.

To setup and configure serverless framework, for deploying the stack using AWS CloudFormation is 
required to execute the following commands on the root folder of the project on a terminal:

```bash
    npm install -g serverless
    serverless config credentials --provider aws --key ${AWS_ACCESS_KEY_ID} --secret ${AWS_SECRET_ACCESS_KEY} --profile demos
```

## Application Setup

To run the application using DynamoDB as datasource:

1. Set environment variables **AWS_ACCESS_KEY_ID** and **AWS_SECRET_ACCESS_KEY** in your .bash_profile to have access to DynamoDB.
2. Ensure that the given AWS credentials have the proper policies to upgrade tables, read and write data in DynamoDB.
3. Download the source code from repository.
4. Go to the root location of the source code.
5. Upgrade DynamoDB tables using serverless.

## Running Application inside IDE

To run the application on your IDE:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Integrate your project with IDE:
    - [Eclipse](http://books.sonatype.com/m2eclipse-book/reference/creating-sect-importing-projects.html)
    - [Netbeans](http://wiki.netbeans.org/MavenBestPractices)
    - [IntelliJ]( https://www.jetbrains.com/idea/help/importing-project-from-maven-model.html)
5. Open IDE and run the class **Application.java**.

## Running Application on Terminal

To run the application on terminal with Maven:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    java -jar target/verspaetung-transport-1.5.0.jar
```

## Running Unit Tests on Terminal

To run the unit tests on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean test
```

## Running Application using Docker

To run the application on terminal with docker:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Verify the version of Docker - 19.x or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean package
    docker build --build-arg AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} --build-arg AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} -t verspaetung-transport .
    docker run -p 8080:8080 verspaetung-transport
```

## Check Application Test Coverage using Jacoco

To verify test coverage on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    open target/site/jacoco/index.html
```

## Testing using Postman Collection

To test using the postman collection:

1. Run the application inside IDE or Terminal.
2. Open Postman application.
3. Import the postman collection **verspaetung-transport-collection.json**.
4. Import the postman environment **verspaetung-transport-environment.json**.
5. Check the environment value of **transport_host** point to localhost and port your application is running.
6. Move the mouse over the collection.
7. Click on the three dots on the right side.
8. Click on **Run collection**.
9. Check the number of iterations is 1.
10. Click on **Run Verspaetung Transport** button.
11. Validate all tests passed.

## Load Test using JMeter

To load test the application using JMeter GUI:

1. Run the application inside IDE or Terminal.
2. Open JMeter GUI.
3. Open **verspaetung-transport.jmx**.
4. Start the load test.

Be aware of the throughput of the application, it depends on the DynamoDB Read Capacity configuration on AWS.

## List Dependency Licenses

To generate list of dependency licenses on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn project-info-reports:dependencies
    open target/site/dependencies.html
```

## Upgrade DynamoDB Tables on AWS using Serverless

After see the test coverage and generate files let's proceed to upgrade the DynamoDB tables to store the transport data:

```bash
    serverless deploy -v --aws-profile demos
```

## Continuous Deployment using Bitbucket Pipelines and Elastic Beanstalk

Deployment to production environment is already configured on **bitbucket-pipelines.yml**. This would create a new environment
on every deployment. 

In order to validate that whole process was make properly execute the following commands:

```bash
    curl -X GET -I http://${URL}/status
    curl -X GET -I http://${URL}/lines/delay/1
    curl -X GET -I http://${URL}/vehicles/1
    curl -X GET -I http://${URL}/vehicles/1/4/10:03
```

To save cost on AWS it is recommended to terminate the environment after the exercise is covered:

```bash
    cd deployment
    eb terminate verspaetung-transport-production
```

## API Documentation

To see swagger documentation:

1. Open a browser
2. Go to http://${URL}/swagger-ui.html

# Contact Information

Email: icsanabriar@googlemail.com