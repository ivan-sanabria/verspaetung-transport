/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bcg.verspaetung.transport.controller;

import com.bcg.verspaetung.transport.dto.Vehicle;
import com.bcg.verspaetung.transport.service.VehicleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Class responsible of handling test cases for vehicles operations:
 *
 * <ul>
 *  <li> find a vehicle for a given x and y at specific time
 *  <li> find a next arriving vehicle at a given stop
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(VehicleController.class)
class VehicleControllerTest {

    /**
     * Mock of vehicle service.
     */
    @MockBean
    private VehicleService vehicleService;

    /**
     * Mock of MVC to perform HTTP requests.
     */
    @Autowired
    private MockMvc mvc;

    /**
     * Object Mapper used to write JSON response data into content bodies.
     */
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void given_coordinate_and_bad_time_return_bad_request() throws Exception {

        mvc.perform(get("/vehicles/2/3/30:53"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void given_coordinate_and_time_return_not_found() throws Exception {

        final Integer x = 2;
        final Integer y = 3;
        final String time = "23:53";

        final List<Vehicle> results = Collections.emptyList();

        given(vehicleService.getVehicleByCoordinateAndTime(x, y, time))
                .willReturn(results);

        mvc.perform(get("/vehicles/2/3/23:53"))
                .andExpect(status().isNotFound());
    }

    @Test
    void given_coordinate_and_time_return_list() throws Exception {

        final Integer x = 2;
        final Integer y = 3;
        final String time = "10:03";

        final Vehicle M3 = new Vehicle(1, "M3", 0);
        final Vehicle S7 = new Vehicle(2, "S7", 2);

        final List<Vehicle> results = Arrays.asList(M3, S7);
        final String expectedResponse = objectMapper.writeValueAsString(results);

        given(vehicleService.getVehicleByCoordinateAndTime(x, y, time))
                .willReturn(results);

        mvc.perform(get("/vehicles/2/3/10:03"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

    @Test
    void given_stop_identifier_return_not_found() throws Exception {

        final Integer stopId = 1;

        final List<Vehicle> results = Collections.emptyList();

        given(vehicleService.getArrivingVehicleByStopIdAndTime(eq(stopId), anyString()))
                .willReturn(results);

        mvc.perform(get("/vehicles/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void given_stop_identifier_return_list() throws Exception {

        final Integer stopId = 2;

        final Vehicle M3 = new Vehicle(1, "M3", 0);
        final Vehicle S7 = new Vehicle(2, "S7", 2);

        final List<Vehicle> results = Arrays.asList(M3, S7);
        final String expectedResponse = objectMapper.writeValueAsString(results);

        given(vehicleService.getArrivingVehicleByStopIdAndTime(eq(stopId), anyString()))
                .willReturn(results);

        mvc.perform(get("/vehicles/2"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

}
