/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bcg.verspaetung.transport.controller;

import com.bcg.verspaetung.transport.service.LineService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Class responsible of test cases for lines operations:
 *
 * <ul>
 *  <li> find a delay line
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(LineController.class)
class LineControllerTest {

    /**
     * Mock of line service.
     */
    @MockBean
    private LineService lineService;

    /**
     * Mock of MVC to perform HTTP requests.
     */
    @Autowired
    private MockMvc mvc;


    @Test
    void given_line_id_return_ok() throws Exception {

        final Integer lineId = 1;

        given(lineService.getLineWithDelayById(lineId))
                .willReturn(true);

        mvc.perform(get("/lines/delay/1"))
                .andExpect(status().isOk());
    }

    @Test
    void given_line_id_return_not_found() throws Exception {

        final Integer lineId = 2;

        given(lineService.getLineWithDelayById(lineId))
                .willReturn(false);

        mvc.perform(get("/lines/delay/2"))
                .andExpect(status().isNotFound());
    }

}
