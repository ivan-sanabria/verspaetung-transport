/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bcg.verspaetung.transport.service;

import com.bcg.verspaetung.transport.domain.Delay;
import com.bcg.verspaetung.transport.domain.Line;
import com.bcg.verspaetung.transport.dto.Vehicle;
import com.bcg.verspaetung.transport.repository.DelayRepository;
import com.bcg.verspaetung.transport.repository.LineRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;

/**
 * Class responsible of handling test cases for the following use cases:
 *
 * <ul>
 *  <li> find a delay line
 *  <li> find vehicles with delay with given identifiers
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@ExtendWith(SpringExtension.class)
class LineServiceTest {

    /**
     * Mock of delay repository.
     */
    @MockBean
    private DelayRepository delayRepository;

    /**
     * Mock of line repository.
     */
    @MockBean
    private LineRepository lineRepository;

    /**
     * Mock of line service.
     */
    private LineService lineService;

    /**
     * Init the necessary mocks before running test cases.
     */
    @BeforeEach
    void initBean() {
        lineService = new LineService(delayRepository, lineRepository);
    }


    @Test
    void given_line_identifier_return_is_delay() {

        final Integer id = 1;
        final String name = "M3";

        final Line line = new Line();

        line.setId(id);
        line.setName(name);

        final Delay delay = new Delay();

        delay.setLine(name);
        delay.setDelay(2);

        given(lineRepository.findById(id))
                .willReturn(Optional.of(line));

        given(delayRepository.findById(name))
                .willReturn(Optional.of(delay));

        assertTrue(lineService.getLineWithDelayById(id));
    }

    @Test
    void given_line_identifier_return_is_not_delay() {

        final Integer notFoundId = 3;

        given(lineRepository.findById(notFoundId))
                .willReturn(Optional.empty());

        assertFalse(lineService.getLineWithDelayById(notFoundId));

        final Integer id = 2;
        final String name = "S7";

        final Line line = new Line();

        line.setId(id);
        line.setName(name);

        given(lineRepository.findById(id))
                .willReturn(Optional.of(line));

        given(delayRepository.findById(name))
                .willReturn(Optional.empty());

        assertFalse(lineService.getLineWithDelayById(id));
    }

    @Test
    void given_line_identifiers_return_lines_map() {

        final Integer M3Id = 1;
        final String M3Name = "M3";

        final Line M3Line = new Line();

        M3Line.setId(M3Id);
        M3Line.setName(M3Name);

        final Delay M3delay = new Delay();

        M3delay.setLine(M3Name);
        M3delay.setDelay(2);

        final Integer S7Id = 2;
        final String S7Name = "S7";

        final Line S7Line = new Line();

        S7Line.setId(S7Id);
        S7Line.setName(S7Name);

        final List<Integer> ids = Arrays.asList(M3Id, S7Id);
        final Iterable<Line> lines = Arrays.asList(M3Line, S7Line);

        given(lineRepository.findAllById(ids))
                .willReturn(lines);

        final Iterable<String> names = Arrays.asList(M3Name, S7Name);
        final Iterable<Delay> delays = Collections.singleton(M3delay);

        given(delayRepository.findAllById(names))
                .willReturn(delays);

        final Map<Integer, Vehicle> results = lineService.getLinesWithDelaysByIds(ids);

        assertEquals(2, results.size());
        assertEquals(new Vehicle(M3Id, M3Name, 2), results.get(M3Id));
        assertEquals(new Vehicle(S7Id, S7Name, 0), results.get(S7Id));
    }

}
