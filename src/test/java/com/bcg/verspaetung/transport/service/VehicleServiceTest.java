/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bcg.verspaetung.transport.service;

import com.bcg.verspaetung.transport.domain.Stop;
import com.bcg.verspaetung.transport.domain.Time;
import com.bcg.verspaetung.transport.dto.Vehicle;
import com.bcg.verspaetung.transport.repository.StopRepository;
import com.bcg.verspaetung.transport.repository.TimeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;

/**
 * Class responsible of handling test cases for the following use cases:
 *
 * <ul>
 *  <li> find vehicle by specific coordinate and time
 *  <li> find next arriving vehicle by stop identifier
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@ExtendWith(SpringExtension.class)
class VehicleServiceTest {

    /**
     * Mock of stop repository.
     */
    @MockBean
    private StopRepository stopRepository;

    /**
     * Mock of time repository.
     */
    @MockBean
    private TimeRepository timeRepository;

    /**
     * Mock of line service.
     */
    @MockBean
    private LineService lineService;

    /**
     * Mock of vehicle service.
     */
    private VehicleService vehicleService;

    /**
     * Init the necessary mocks before running test cases.
     */
    @BeforeEach
    void initBean() {
        vehicleService = new VehicleService(stopRepository, timeRepository, lineService);
    }


    @Test
    void given_coordinate_and_time_return_vehicles() {

        final Integer x = 4;
        final Integer y = 3;
        final String givenTime = "14:05";

        final Stop stopA = new Stop();

        stopA.setId(1);
        stopA.setX(1);
        stopA.setY(1);

        final Stop stopB = new Stop();

        stopB.setId(2);
        stopB.setX(1);
        stopB.setY(6);

        final Stop stopC = new Stop();

        stopC.setId(3);
        stopC.setX(4);
        stopC.setY(4);

        final Stop stopD = new Stop();

        stopD.setId(6);
        stopD.setX(6);
        stopD.setY(2);

        final List<Stop> stops = Arrays.asList(stopA, stopB, stopC, stopD);

        given(stopRepository.findAll())
                .willReturn(stops);

        final Integer lineId = 1;

        final Time time = new Time();

        time.setId(1);
        time.setStopId(3);
        time.setLineId(lineId);
        time.setTime(givenTime);

        final List<Time> times = Collections.singletonList(time);

        given(timeRepository.findAllByStopId(stopC.getId()))
                .willReturn(times);

        final List<Integer> lineIds = Collections.singletonList(lineId);

        final Vehicle vehicle = new Vehicle(lineId, "M4", 0);

        given(lineService.getLinesWithDelaysByIds(lineIds))
                .willReturn(Collections.singletonMap(1, vehicle));

        final List<Vehicle> results = vehicleService
                .getVehicleByCoordinateAndTime(x, y, givenTime);

        assertEquals(1, results.size());
        assertEquals(vehicle, results.get(0));
    }

    @Test
    void given_coordinate_and_time_return_empty_list() {

        final Integer x = 4;
        final Integer y = 3;
        final String givenTime = "14:05";

        given(stopRepository.findAll())
                .willReturn(Collections.emptyList());

        final List<Vehicle> results = vehicleService
                .getVehicleByCoordinateAndTime(x, y, givenTime);

        assertTrue(results.isEmpty());
    }

    @Test
    void given_stop_identifier_and_time_return_vehicles() {

        final Integer stopId = 1;
        final String givenTime = "14:07:00";

        final Integer M4Id = 1;
        final Integer M10Id = 2;
        final Integer S42Id = 3;

        final Time M4Time = new Time();

        M4Time.setId(1);
        M4Time.setStopId(stopId);
        M4Time.setLineId(M4Id);
        M4Time.setTime("14:07:02");

        final Time M10Time = new Time();

        M10Time.setId(2);
        M10Time.setStopId(stopId);
        M10Time.setLineId(M10Id);
        M10Time.setTime("14:03:02");

        final Time S42Time = new Time();

        S42Time.setId(3);
        S42Time.setStopId(stopId);
        S42Time.setLineId(S42Id);
        S42Time.setTime("14:00:02");

        final List<Time> times = Arrays.asList(M4Time, M10Time, S42Time);

        given(timeRepository.findAllByStopId(stopId))
                .willReturn(times);

        final List<Integer> lineIds = Arrays.asList(M4Id, M10Id, S42Id);

        final Vehicle M4 = new Vehicle(M4Id, "M4", 0);
        final Vehicle M10 = new Vehicle(M10Id, "M10", 3);
        final Vehicle S42 = new Vehicle(S42Id, "S42", 7);

        final Map<Integer, Vehicle> arrivingLines = new HashMap<>();

        arrivingLines.put(M4Id, M4);
        arrivingLines.put(M10Id, M10);
        arrivingLines.put(S42Id, S42);

        given(lineService.getLinesWithDelaysByIds(lineIds))
                .willReturn(arrivingLines);

        final List<Vehicle> results = vehicleService.getArrivingVehicleByStopIdAndTime(stopId, givenTime);

        assertEquals(2, results.size());
        assertEquals(M4, results.get(0));
        assertEquals(S42, results.get(1));
    }

    @Test
    void given_stop_identifier_and_time_return_empty_list() {

        final Integer stopId = 1;
        final String givenTime = "11:00:00";

        final Integer M4Id = 1;
        final Integer S42Id = 2;

        final Time M4Time = new Time();

        M4Time.setId(1);
        M4Time.setStopId(stopId);
        M4Time.setLineId(M4Id);
        M4Time.setTime("10:07:02");

        final Time S42Time = new Time();

        S42Time.setId(2);
        S42Time.setStopId(stopId);
        S42Time.setLineId(S42Id);
        S42Time.setTime("10:00:02");

        final List<Time> times = Arrays.asList(M4Time, S42Time);

        given(timeRepository.findAllByStopId(stopId))
                .willReturn(times);

        final List<Integer> lineIds = Arrays.asList(M4Id, S42Id);

        final Vehicle M4 = new Vehicle(M4Id, "M4", 1);
        final Vehicle S42 = new Vehicle(S42Id, "S42", 3);

        final Map<Integer, Vehicle> arrivingLines = new HashMap<>();

        arrivingLines.put(M4Id, M4);
        arrivingLines.put(S42Id, S42);

        given(lineService.getLinesWithDelaysByIds(lineIds))
                .willReturn(arrivingLines);

        final List<Vehicle> results = vehicleService.getArrivingVehicleByStopIdAndTime(stopId, givenTime);

        assertTrue(results.isEmpty());
    }

}
