/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bcg.verspaetung.transport.controller;


import com.bcg.verspaetung.transport.dto.Vehicle;
import com.bcg.verspaetung.transport.service.VehicleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class responsible of handling request made to /vehicles endpoint supporting:
 *
 * <ul>
 *  <li> find a vehicle for a given x and y at specific time
 *  <li> find a next arriving vehicle at a given stop
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RestController
@Tag(name = "Vehicle Endpoints", description = "Supported operations to query vehicles data.")
public class VehicleController {

    /**
     * Time pattern to validate 24 hours format.
     */
    private static final String TIME_PATTERN = "([01]?[\\d]|2[0-3]):[0-5][\\d]";

    /**
     * Service to expose vehicle searches.
     */
    private final VehicleService vehicleService;

    /**
     * Pattern used to validate the time inputs.
     */
    private final Pattern pattern;

    /**
     * Constructor of the vehicle controller to inject required services.
     *
     * @param vehicleService Vehicle service instance used to handle vehicle searches.
     */
    @Autowired
    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
        this.pattern = Pattern.compile(TIME_PATTERN);
    }

    /**
     * Method to handle get request to /vehicles/x/y/time endpoint.
     *
     * @param x    Value of the coordinate.
     * @param y    Value of the coordinate.
     * @param time Time to make the search. (00:00-23:59)
     * @return Response entity with the vehicle data, otherwise NOT_FOUND is returned.
     */
    @Operation(summary = "Find a list of vehicles for a given x and y at specific time.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully found a list of vehicles on the system.",
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = Vehicle.class)))),
            @ApiResponse(responseCode = "400",
                    description = "The given coordinates or time are not parsable.",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "A vehicle with the given (x,y) and time was not found.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the vehicle request.",
                    content = @Content)
    })
    @GetMapping(value = "/vehicles/{x}/{y}/{time}", produces = "application/json")
    public ResponseEntity<List<Vehicle>> getVehicleForCoordinateAndTime(@Parameter(description = "X of a coordinate.")
                                                                        @PathVariable(value = "x") Integer x,
                                                                        @Parameter(description = "Y of a coordinate.")
                                                                        @PathVariable(value = "y") Integer y,
                                                                        @Parameter(description = "Time with format HH:mm (22:10).")
                                                                        @PathVariable(value = "time") String time) {

        final Matcher matcher = pattern.matcher(time);

        if (!matcher.matches())
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        final List<Vehicle> vehicleList = vehicleService.getVehicleByCoordinateAndTime(x, y, time);

        if (vehicleList.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(vehicleList, HttpStatus.OK);
    }

    /**
     * Method to handle get request to /vehicles/stopId endpoint.
     *
     * @param stopId Stop identifier to make the search.
     * @return Response entity with the vehicle data, otherwise NOT_FOUND is returned.
     */
    @Operation(summary = "Find a list of vehicles for a given stop identifier.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully found a list of vehicles on the system.",
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = Vehicle.class)))),
            @ApiResponse(responseCode = "400",
                    description = "The given stop identifier is not parsable.",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "A vehicle with the given stop data was not found.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the vehicle request.",
                    content = @Content),
    })
    @GetMapping(value = "/vehicles/{stopId}", produces = "application/json")
    public ResponseEntity<List<Vehicle>> getNextArrivalAtStop(@Parameter(description = "Identifier of a stop.")
                                                              @PathVariable(value = "stopId") Integer stopId) {

        final String time = LocalDateTime.now()
                .format(VehicleService.FORMATTER);

        final List<Vehicle> vehicleList = vehicleService.getArrivingVehicleByStopIdAndTime(stopId, time);

        if (vehicleList.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else
            return new ResponseEntity<>(vehicleList, HttpStatus.OK);
    }

}
