/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bcg.verspaetung.transport.controller;

import com.bcg.verspaetung.transport.service.LineService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class responsible of handling request made to /lines endpoint supporting:
 *
 * <ul>
 *  <li> find a delay line
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RestController
@Tag(name = "Line Endpoints", description = "Supported operations to query line data.")
public class LineController {

    /**
     * Service to expose line searches.
     */
    private final LineService lineService;

    /**
     * Constructor of the line controller to inject required services.
     *
     * @param lineService Line service instance used to handle line searches.
     */
    @Autowired
    public LineController(LineService lineService) {
        this.lineService = lineService;
    }

    /**
     * Method to handle get request to /lines/delay/id endpoint.
     *
     * @param id Line identifier to make the search.
     * @return Response entity with the line data, otherwise NOT_FOUND is returned.
     */
    @Operation(summary = "Find a delay line for a given identifier.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully found the line with the given identifier as delay in the system.",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "The given line identifier is not parsable.",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "A line with the given identifier was not found as delay.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the line request.",
                    content = @Content),
    })
    @GetMapping(value = "/lines/delay/{id}")
    public ResponseEntity<Void> getDelayLineById(@Parameter(description = "Identifier of a line.")
                                                 @PathVariable(value = "id") Integer id) {

        final boolean delayed = lineService.getLineWithDelayById(id);

        if (delayed)
            return new ResponseEntity<>(HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
