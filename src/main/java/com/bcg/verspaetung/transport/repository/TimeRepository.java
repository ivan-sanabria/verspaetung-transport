/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bcg.verspaetung.transport.repository;

import com.bcg.verspaetung.transport.domain.Time;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Class responsible of handling time persistence against dynamo db.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@EnableScan
@EnableScanCount
public interface TimeRepository extends PagingAndSortingRepository<Time, Integer> {

    /**
     * Find all the lines that pass at the given stop identifier.
     *
     * @param stopId Stop identifier to find the lines.
     * @return Find all the lines that pass by the given stop.
     */
    List<Time> findAllByStopId(Integer stopId);

}
