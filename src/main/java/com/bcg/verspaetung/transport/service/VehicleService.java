/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bcg.verspaetung.transport.service;

import com.bcg.verspaetung.transport.domain.Stop;
import com.bcg.verspaetung.transport.domain.Time;
import com.bcg.verspaetung.transport.dto.Vehicle;
import com.bcg.verspaetung.transport.repository.StopRepository;
import com.bcg.verspaetung.transport.repository.TimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Class responsible of handling vehicles business logic covering the following use cases:
 *
 * <ul>
 *  <li> find vehicle by specific coordinate and time
 *  <li> find next arriving vehicle by stop identifier
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Service
public class VehicleService {

    /**
     * Formatter of the time records.
     */
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");

    /**
     * Stop repository used to persist data into dynamo db.
     */
    private final StopRepository stopRepository;

    /**
     * Time repository used to persist data into dynamo db.
     */
    private final TimeRepository timeRepository;

    /**
     * Line service used to query data from dynamo db.
     */
    private final LineService lineService;

    /**
     * Constructor of the vehicle service used to inject repository.
     *
     * @param stopRepository Stop repository to persist data into dynamo db.
     * @param timeRepository Time repository to persist data into dynamo db.
     * @param lineService    Line service to query data from dynamo db.
     */
    @Autowired
    public VehicleService(StopRepository stopRepository, TimeRepository timeRepository, LineService lineService) {
        this.stopRepository = stopRepository;
        this.timeRepository = timeRepository;
        this.lineService = lineService;
    }

    /**
     * Find a vehicle by specific coordinate and time.
     *
     * @param x    X position of the coordinate.
     * @param y    Y position of the coordinate.
     * @param time Time to find the vehicle.
     * @return List of Vehicle transfer objects containing the found vehicles. In case there is none, empty list is returned.
     */
    public List<Vehicle> getVehicleByCoordinateAndTime(final Integer x, final Integer y, final String time) {

        final Iterable<Stop> stops = stopRepository.findAll();

        final Optional<Map.Entry<Integer, Double>> closestPoint = StreamSupport.stream(stops.spliterator(), false)
                .map(t -> {

                    final double dx = (double) (t.getX() - x);
                    final double dy = (double) (t.getY() - y);

                    final double squares = Math.pow(dx, 2) + Math.pow(dy, 2);
                    final double distance = Math.sqrt(squares);

                    return Map.entry(t.getId(), distance);

                }).min(Comparator.comparingDouble(Map.Entry::getValue));

        if (closestPoint.isPresent()) {

            final Integer identifier = closestPoint.get()
                    .getKey();

            final DateTimeFormatter originalFormat = DateTimeFormatter.ofPattern("HH:mm");

            final String searchTime = FORMATTER.format(
                    originalFormat.parse(time));

            return getArrivingVehicleByStopIdAndTime(
                    identifier,
                    searchTime);
        }

        return new ArrayList<>();
    }

    /**
     * Find arriving vehicles to the given stop identifier.
     *
     * @param stopId Stop identifier to find a arriving vehicle.
     * @param time   Time to find arrival to given stop.
     * @return List of Vehicle transfer objects containing the arriving vehicles. In case there is none, empty list is returned.
     */
    public List<Vehicle> getArrivingVehicleByStopIdAndTime(final Integer stopId, final String time) {

        List<Time> times = timeRepository.findAllByStopId(stopId);

        final List<Integer> arrivingIds = times.stream()
                .map(Time::getLineId)
                .collect(Collectors.toList());

        final Map<Integer, Vehicle> arrivingLines = lineService.getLinesWithDelaysByIds(arrivingIds);

        times.forEach(t -> {

            final Integer lineDelay = arrivingLines.get(
                    t.getLineId())
                    .getDelay();

            final LocalTime lineTime = LocalTime.parse(
                    t.getTime())
                    .plusMinutes(lineDelay);

            t.setTime(lineTime.format(FORMATTER));
        });

        final Optional<Time> bestTime = findClosestTime(time, times);

        if (bestTime.isPresent()) {

            final List<Integer> matchingIds = times.stream()
                    .filter(t ->
                            bestTime.get()
                                    .getTime()
                                    .equals(t.getTime()))
                    .map(Time::getLineId)
                    .collect(Collectors.toList());

            return arrivingLines.values()
                    .stream()
                    .filter(t ->
                            matchingIds.contains(t.getId()))
                    .collect(Collectors.toList());
        }

        return new ArrayList<>();
    }

    /**
     * Find the closest time of the list to the given time. Format of the time is HH:mm:ss.
     *
     * @param time  Time to look closest one on the given list.
     * @param times List of times to search the closest one of the given time.
     * @return Optional that is closest to the given time.
     */
    private Optional<Time> findClosestTime(final String time, final List<Time> times) {

        final LocalTime givenTime = LocalTime.parse(time);

        return times.stream()
                .filter(t -> {

                    final LocalTime lineTime = LocalTime.parse(
                            t.getTime());

                    return ChronoUnit.SECONDS.between(givenTime, lineTime) >= 0;

                }).min(Comparator.comparing(Time::getTime));
    }

}
