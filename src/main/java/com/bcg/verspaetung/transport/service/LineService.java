/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bcg.verspaetung.transport.service;

import com.bcg.verspaetung.transport.domain.Delay;
import com.bcg.verspaetung.transport.domain.Line;
import com.bcg.verspaetung.transport.dto.Vehicle;
import com.bcg.verspaetung.transport.repository.DelayRepository;
import com.bcg.verspaetung.transport.repository.LineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Spliterator;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Class responsible of handling line business logic covering the following use cases:
 *
 * <ul>
 *  <li> find a delay line
 *  <li> find vehicles with delay with given identifiers
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Service
public class LineService {

    /**
     * Delay repository used to persist data into dynamo db.
     */
    private final DelayRepository delayRepository;

    /**
     * Line repository used to persist data into dynamo db.
     */
    private final LineRepository lineRepository;

    /**
     * Constructor of the line service used to inject repository.
     *
     * @param delayRepository Delay repository to persist data into dynamo db.
     * @param lineRepository  Line repository to persist data into dynamo db.
     */
    @Autowired
    public LineService(DelayRepository delayRepository, LineRepository lineRepository) {
        this.delayRepository = delayRepository;
        this.lineRepository = lineRepository;
    }

    /**
     * Find if a given line is delay or not.
     *
     * @param id Identifier of the line.
     * @return True when the given line identifier is delay, otherwise is false.
     */
    public boolean getLineWithDelayById(final Integer id) {

        final Optional<Line> optionalLine = lineRepository.findById(id);

        if (optionalLine.isPresent()) {

            final Line line = optionalLine.get();

            final Optional<Delay> delay = delayRepository.findById(
                    line.getName());

            return delay.isPresent();
        }

        return false;
    }

    /**
     * Find lines with delays using line identifiers.
     *
     * @param ids Identifier of lines to filter the search.
     * @return List of lines with delay by the given identifiers.
     */
    Map<Integer, Vehicle> getLinesWithDelaysByIds(final List<Integer> ids) {

        final Iterable<Line> lines = lineRepository.findAllById(ids);

        final List<String> names = StreamSupport.stream(lines.spliterator(), false)
                .map(Line::getName)
                .collect(Collectors.toList());

        final Iterable<Delay> delays = delayRepository.findAllById(names);
        final Spliterator<Delay> delayIterator = delays.spliterator();

        final Map<String, Delay> cacheDelays = StreamSupport.stream(delayIterator, false)
                .collect(Collectors.toMap(Delay::getLine, delay -> delay));

        return StreamSupport.stream(lines.spliterator(), false)
                .map(l -> {

                    final String name = l.getName();

                    final Integer delay = cacheDelays.containsKey(name) ?
                            cacheDelays.get(name).getDelay() : 0;

                    return new Vehicle(
                            l.getId(),
                            name,
                            delay);

                }).collect(Collectors.toMap(Vehicle::getId, vehicle -> vehicle));
    }

}
