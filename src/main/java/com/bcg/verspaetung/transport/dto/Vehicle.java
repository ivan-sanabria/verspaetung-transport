/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bcg.verspaetung.transport.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class to encapsulate the vehicle data supported by the vehicles endpoints.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Vehicle data transfer object used to establish communication between the clients " +
        "and backend services.")
public class Vehicle {

    /**
     * Name of the vehicle.
     */
    @Schema(description = "Identifier of the vehicle",
            example = "1")
    private Integer id;

    /**
     * Name of the vehicle.
     */
    @Schema(description = "Name of the vehicle",
            example = "200")
    private String name;

    /**
     * Delay of the vehicle
     */
    @Schema(description = "Delay of the vehicle in minutes",
            example = "3")
    private Integer delay;

}
