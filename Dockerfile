FROM adoptopenjdk/openjdk14:ubi

LABEL maintainer="icsanabriar@googlemail.com"

ARG AWS_ACCESS_KEY_ID=""
ENV AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID

ARG AWS_SECRET_ACCESS_KEY=""
ENV AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY

ARG JAR_FILE=target/verspaetung-transport-*.jar
ADD ${JAR_FILE} verspaetung-transport.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/verspaetung-transport.jar"]

EXPOSE 8080